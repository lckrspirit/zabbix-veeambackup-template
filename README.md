#### На целевом сервере
1) Создать каталог в разделе C:\zabbix_folder\scripts
2) Перенести скрипты - `BackupJobDiscovery.ps1` и `GetJobState.ps1` в созданный каталог.
3) В конфиге заббикс-агента, нужно добавить параметры:
```
Timeout=30
UserParameter=vbr.jobs,powershell -NoProfile -ExecutionPolicy Bypass -File "C:\zabbix\scripts\BackupJobDiscovery.ps1"
UserParameter=job.status[*],powershell -NoProfile -ExecutionPolicy Bypass -File "C:\zabbix\scripts\GetJobState.ps1" $1 
```
4) Перезапускаем службу - `zabbix-agent`

#### На zabbix-сервере
1) Импортировать шаблон в заббикс сервер, повесить шаблон на хост. 
